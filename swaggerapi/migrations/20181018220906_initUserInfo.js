
exports.up = function (knex, Promise) {
    return knex.schema.createTable('userInfo', function (t) {
        t.increments('id').unsigned().primary();
        t.string('email').notNull().unique();
        t.string('canvasAccessToken').notNull();
        t.dateTime('createdAt').notNull().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
        t.dateTime('updatedAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
    });
};

exports.down = function (knex, Promise) {
    return knex.schema.dropTable('userInfo');
};
