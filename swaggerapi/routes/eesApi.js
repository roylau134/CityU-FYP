'use strict';
const express = require('express');
const router = express.Router();
const EesApiController = require("../ees/controller/ees-apiController.js");
const responseHelper = require("../ees/response/responseHelper.js");

const os = require("os");
const uuid = require('uuid');
const processUuid = uuid.v4();
module.exports = (app) => {
    const router = express.Router();
    const eesApiController = new EesApiController();
    /**
     * @swagger
     * tags:
     *   name: EEs-API
     *   description: Electoric Eduaction System API
     */

    /**
    * @swagger
    * /api/email:
    *   post:
    *     tags:
    *      - EEs-API
    *     description: get email by canvas access token
    *     produces:
    *     - application/json
    *     parameters:
    *     - name: canvasAccessToken
    *       description: canvas access token
    *       in: formData
    *       required: true
    *       type: string
    *     responses:
    *       200:
    *         description: Success!
    */

    router.post("/email", checkApiKey, function (req, res) {
        const canvasAccessToken = req.body.canvasAccessToken;
        eesApiController.getEmail(canvasAccessToken).then(function (response) {
            res.send(response);
        });
    });

    /**
    * @swagger
    * /api/signup:
    *   post:
    *     tags:
    *      - EEs-API
    *     description: sign up api
    *     produces:
    *     - application/json
    *     parameters:
    *     - name: email
    *       description: cityu email address
    *       in: formData
    *       required: true
    *       type: string
    *     - name: canvasAccessToken
    *       description: canvas access token
    *       in: formData
    *       required: true
    *       type: string
    *     - name: password
    *       description: password for new register
    *       in: formData
    *       required: true
    *       type: string
    *     responses:
    *       200:
    *         description: Success!
    */
    router.post("/signup", checkApiKey, function (req, res) {
        const email = req.body.email;
        const accessToken = req.body.canvasAccessToken;
        const password = req.body.password;
        eesApiController.signUpForNewUser(email, accessToken, password).then(function (response) {
            res.send(response);
        })
    });

    /**
       * @swagger
       * /api/user:
       *   post:
       *     tags:
       *      - EEs-API
       *     description: get the user info
       *     produces:
       *     - application/json
       *     parameters:
       *     - name: jwt
       *       description: Json Web Token
       *       in: formData
       *       required: true
       *       type: string
       *     responses:
       *       200:
       *         description: Success!
       */
    router.post("/user", checkApiKey, function (req, res) {
        const jwt = req.body.jwt;
        eesApiController.getUserInfo(jwt).then(function (response) {
            res.send(response);
        })
    });

    /**
       * @swagger
       * /api/course:
       *   get:
       *     tags:
       *      - EEs-API
       *     description: get course ofthe user
       *     produces:
       *     - application/json
       *     parameters:
       *     - name: jwt
       *       description: Json Web Token
       *       in: query
       *       required: true
       *       type: string
       *     responses:
       *       200:
       *         description: Success!
       */
    router.get("/course", checkApiKey, function (req, res) {
        const jwt = req.query.jwt;
        eesApiController.getCourse(jwt).then(function (response) {
            res.send(response);
        })
    });

    /**
       * @swagger
       * /api/getUserOfCourse:
       *   get:
       *     tags:
       *      - EEs-API
       *     description: get course ofthe user
       *     produces:
       *     - application/json
       *     parameters:
       *     - name: jwt
       *       description: Json Web Token
       *       in: query
       *       required: true
       *       type: string
       *     - name: courseId
       *       description: the course Id
       *       in: query
       *       required: true
       *       type: string
       *     responses:
       *       200:
       *         description: Success!
       */
    router.get("/getUserOfCourse", checkApiKey, function (req, res) {
        const jwt = req.query.jwt;
        const courseId = req.query.courseId;
        eesApiController.getUserOfCourse(jwt, courseId).then(function (response) {
            res.send(response);
        })
    });

    /**
           * @swagger
           * /api/getCourseById:
           *   get:
           *     tags:
           *      - EEs-API
           *     description: get course ofthe user
           *     produces:
           *     - application/json
           *     parameters:
           *     - name: jwt
           *       description: Json Web Token
           *       in: query
           *       required: true
           *       type: string
           *     - name: courseId
           *       description: the course Id
           *       in: query
           *       required: true
           *       type: string
           *     responses:
           *       200:
           *         description: Success!
           */
    router.get("/getCourseById", checkApiKey, function (req, res) {
        const jwt = req.query.jwt;
        const courseId = req.query.courseId;
        eesApiController.getCourseById(jwt, courseId).then(function (response) {
            res.send(response);
        })
    });

    /**
    * @swagger
    * /api/gradeColumns:
    *   get:
    *     tags:
    *      - EEs-API
    *     description: get grade column of the course
    *     produces:
    *     - application/json
    *     parameters:
    *     - name: jwt
    *       description: Json Web Token
    *       in: query
    *       required: true
    *       type: string
    *     - name: courseId
    *       description: the course Id
    *       in: query
    *       required: true
    *       type: string
    *     responses:
    *       200:
    *         description: Success!
    */
    router.get("/gradeColumns", checkApiKey, function (req, res) {
        const jwt = req.query.jwt;
        const courseId = req.query.courseId;
        eesApiController.getGradeColumnByCourseId(jwt, courseId).then(function (response) {
            res.send(response);
        })
    });

    /**
    * @swagger
    * /api/grade:
    *   get:
    *     tags:
    *      - EEs-API
    *     description: get grade of the course
    *     produces:
    *     - application/json
    *     parameters:
    *     - name: jwt
    *       description: Json Web Token
    *       in: query
    *       required: true
    *       type: string
    *     - name: courseId
    *       description: the course Id
    *       in: query
    *       required: true
    *       type: string
    *     - name: studentId
    *       description: the student Id array
    *       in: query
    *       required: true
    *       type: string
    *     responses:
    *       200:
    *         description: Success!
    */
    router.get("/grade", checkApiKey, function (req, res) {
        const jwt = req.query.jwt;
        const courseId = req.query.courseId;
        const studentId = req.query.studentId;
        eesApiController.getGradeByCourseIdAndStudentId(jwt, courseId, studentId).then(function (response) {
            res.send(response);
        })
    });

    /**
    * @swagger
    * /api/conversation:
    *   post:
    *     tags:
    *      - EEs-API
    *     description: submit a conversation
    *     produces:
    *     - application/json
    *     parameters:
    *     - name: jwt
    *       description: Json Web Token
    *       in: query
    *       required: true
    *       type: string
    *     - name: courseId
    *       description: the course Id
    *       in: query
    *       required: true
    *       type: string
    *     - name: studentId
    *       description: the student Id array
    *       in: query
    *       required: true
    *       type: string
    *     - name: subject
    *       description: the subject of the conversation
    *       in: query
    *       required: true
    *       type: string
    *     - name: body
    *       description: the body of the conversation
    *       in: query
    *       required: true
    *       type: string
    *     responses:
    *       200:
    *         description: Success!
    */
    router.post("/conversation", checkApiKey, function (req, res) {
        const jwt = req.query.jwt;
        const courseId = req.query.courseId;
        const studentId = req.query.studentId;
        const subject = req.query.subject;
        const body = req.query.body;
        eesApiController.postConversation(jwt, courseId, studentId, subject, body).then(function (response) {
            res.send(response);
        })
    });

    function checkApiKey(req, res, next) {
        if (req.headers.api_key && req.headers.api_key === "123456") {
            next(); // allow the next route to run
        } else {
            res.status(404);
            res.send(responseHelper.authErrorResponse());
        }
    }

    return router;
}
