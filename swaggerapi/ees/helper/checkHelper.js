"use strict";

module.exports = function () {
    function isCharacter(string = "") {
        const pattern = /^\w+$/;
        return pattern.test(string);
    }

    function isDigit(string = "") {
        const pattern = /^\d+$/;
        return pattern.test(string);
    }

    function isCharacterOrDigitOnly(string = "") {
        return isCharacter(string) || isDigit(string);
    }

    function isCharacterOrDigit(string = "") {
        const pattern = /^[\d\w]+$/;
        return pattern.test(string);
    }

    function isForAccessToken(string = "") {
        const pattern = /^[\w\d~]+$/;
        return pattern.test(string);
    }

    this.isCharacter = (string) => {
        return isCharacter(string);
    }

    this.isDigit = (string) => {
        return isDigit(string);
    }

    this.isCharacterOrDigit = (string) => {
        return isCharacterOrDigit(string);
    }

    this.isCharacterOrDigitOnly = (string) => {
        return isCharacterOrDigitOnly(string);
    }

    this.isForAccessToken = (string) => {
        return isForAccessToken(string);
    }
}