const mysql = require('mysql');
const config = require("../static/webDbConfig.json");

module.exports = function () {
    function queryDB(query = "", queryData = null) {
        return new Promise(function (reslove, reject) {
            const connection = createConnection();
            connection.query(query, queryData, function (error, results, fields) {
                if (error) {
                    reject(error);
                }
                reslove(results);
            });
            connection.end();
        });
    }

    this.query = (query, queryData) => {
        return queryDB(query, queryData);
    }

    function createConnection() {
        const connection = mysql.createConnection({
            host: config.host,
            user: config.user,
            password: config.password,
            database: config.database
        });
        connection.connect(function (err) {
            if (err) {
                console.error('error connecting: ' + err.stack);
                throw err;
            }
            console.log(`Database connected as id ${connection.threadId}`);
        });
        return connection;
    }
}