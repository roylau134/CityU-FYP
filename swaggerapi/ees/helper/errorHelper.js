"use stricet";

class DomainError extends Error {
    constructor(message) {
        super(message);
        // Ensure the name of this error is the same as the class name
        this.name = this.constructor.name;
        // This clips the constructor invocation from the stack trace.
        // It's not absolutely essential, but it does make the stack trace a little nicer.
        //  @see Node.js reference (bottom)
        Error.captureStackTrace(this, this.constructor);
    }
}

class ResourceNotFoundError extends DomainError {
    constructor(resource, query) {
        super(`Resource ${resource} was not found.`);
        this.data = { resource, query };
    }
}

// I do something like this to wrap errors from other frameworks.
// Correction thanks to @vamsee on Twitter:
// https://twitter.com/lakamsani/status/1035042907890376707
class InternalError extends DomainError {
    constructor(error) {
        super(error.message);
        this.data = { error };
    }
}

class AccessTokenNotCorrect extends DomainError {
    constructor(accessToken) {
        super(`Your access token, "${accessToken}" is not correct.`);
        this.data = { accessToken };
    }
}

class ApiOptionIsEmpty extends DomainError {
    constructor(error) {
        super(error.message);
        this.data = { error };
    }
}

class CanvasCannotAccess extends DomainError {
    constructor(error) {
        super(error.message);
        this.data = { error };
    }
}

class InputIsMissing extends DomainError {
    constructor(error, inputArray = []) {
        let displayMessage;
        for (const index in inputArray) {
            const name = inputArray[index];
            displayMessage + `The ${name} is missing\n`;
        }
        super(error.message + `\n${displayMessage}`);
        this.data = { inputArray };
    }
}

class InputError extends DomainError {
    constructor(error, inputJson = {}) {
        super(error.message + `\n${JSON.stringify(inputJson)}`);
        this.data = { inputJson };
    }
}

class QueryParameterIsMissing extends DomainError {
    constructor(error, inputJson = []) {
        super(error.message + `\n${JSON.stringify(inputJson)}`);
        this.data = { inputJson };
    }
}

class DatabaseError extends DomainError {
    constructor(error) {
        super(error.message);
        this.data = { error };
    }
}

class DatabaseDataNotFopund extends DomainError {
    constructor(error) {
        super(error.message);
        this.data = { error };
    }
}

module.exports = {
    ResourceNotFoundError,
    InternalError,
    AccessTokenNotCorrect,
    ApiOptionIsEmpty,
    CanvasCannotAccess,
    InputIsMissing,
    InputError,
    QueryParameterIsMissing,
    DatabaseError,
    DatabaseDataNotFopund
};