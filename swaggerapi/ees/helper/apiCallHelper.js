"use strict";
const errorHelper = require("../helper/errorHelper.js");


module.exports = function () {
    function apiCall(http, options = null, data = {}) {
        return new Promise(function (resolve, reject) {
            if (options == null) {
                reject(new errorHelper.ApiOptionIsEmpty);
            }
            else {
                console.log(`Call external API: "${JSON.stringify(options)}"`);
                const request = http.request(options, function (res) {
                    res.on("data", function (result) {
                        try {
                            const jsonResult = JSON.parse(result.toString());
                            resolve(jsonResult);
                        } catch (error) {
                            console.error(error);
                            resolve({});
                        }
                    });
                })

                request.on('error', function (err) {
                    reject(err);
                });

                request.write(JSON.stringify(data));

                request.end();
            }
        })
    }

    function httpsApiCall(options, data = {}) {
        if (!options) {
            throw new errorHelper.ApiOptionIsEmpty;
        }
        else {
            const https = require('https');
            return apiCall(https, options, data);
        }
    }

    function httpApiCall(options, data = {}) {
        if (!options) {
            throw new errorHelper.ApiOptionIsEmpty;
        }
        else {
            const http = require('http');
            return apiCall(http, options, data);
        }
    }

    function apiOption(host = null, path = null, port = null, method = "GET", headers = {}) {
        if (host && path && method) {
            return {
                "host": host,
                "path": path,
                "port": port,
                "method": method,
                "headers": headers
            }
        } else {
            return {};
        }
    }

    this.httpsApiCall = (options, data) => {
        return httpsApiCall(options, data);
    }

    this.httpApiCall = (options, data) => {
        return httpApiCall(options, data);
    }

    this.apiOption = (host, path, port, method, headers) => {
        return apiOption(host, path, port, method, headers);
    }
}