"use strict";
const CanvasAO = require("../accessObject/canvasAO.js");
const errorHelper = require("../helper/errorHelper.js");

module.exports = function () {
    const canvasAO = new CanvasAO();

    function getEmail(accessToken = "") {
        return new Promise(function (reslove, reject) {
            canvasAO.getUserInfo(accessToken).then(function (result) {
                if (!result.errors) {//result contain no error
                    if (result.primary_email) {
                        reslove(result.primary_email);
                    }
                    else {//contain no email address
                        reslove("");
                    }
                }
                else if (result.errors[0].message == "Invalid access token.") {
                    reject(new errorHelper.AccessTokenNotCorrect(accessToken));
                }
                reject(new errorHelper.InternalError(new Error("internal error")));
            }).catch(function (error) {
                reject(error);
            });
        });
    }

    this.getEmail = (accessToken) => {
        return getEmail(accessToken);
    }

    function getUserName(accessToken = "") {
        return new Promise(function (reslove, reject) {
            canvasAO.getUserInfo(accessToken).then(function (result) {
                if (!result.errors) {//result contain no error
                    if (result.sortable_name) {
                        reslove(result.sortable_name);
                    }
                    else {//contain no name
                        reslove("");
                    }
                }
                else if (result.errors[0].message == "Invalid access token.") {
                    reject(new errorHelper.AccessTokenNotCorrect(accessToken));
                }
                reject(new errorHelper.InternalError(new Error("internal error")));
            }).catch(function (error) {
                reject(error);
            });
        });
    };

    this.getUserName = (accessToken = "") => {
        return getUserName(accessToken);
    }

    function getCourse(accessToken = "") {
        return new Promise(function (reslove, reject) {
            canvasAO.getCourse(accessToken).then(function (result) {
                if (!result.errors) {//result contain no error
                    if (result) {
                        reslove(result);
                    }
                    else {//contain nothing
                        reslove([]);
                    }
                }
                else if (result.errors[0].message == "Invalid access token.") {
                    reject(new errorHelper.AccessTokenNotCorrect(accessToken));
                }
                reject(new errorHelper.InternalError(new Error("internal error")));
            }).catch(function (error) {
                reject(error);
            });
        });
    };

    this.getCourse = (accessToken = "") => {
        return getCourse(accessToken);
    }

    function getUserOfCourse(accessToken = "", courseId = null) {
        return new Promise(function (reslove, reject) {
            canvasAO.getUserOfCourse(accessToken, courseId).then(function (result) {
                if (!result.errors) {//result contain no error
                    if (result) {
                        reslove(result);
                    }
                    else {//contain nothing
                        reslove([]);
                    }
                }
                else if (result.errors[0].message == "Invalid access token.") {
                    reject(new errorHelper.AccessTokenNotCorrect(accessToken));
                }
                reject(new errorHelper.InternalError(new Error("internal error")));
            }).catch(function (error) {
                reject(error);
            });
        });
    };

    this.getUserOfCourse = (accessToken = "", courseId = null) => {
        return getUserOfCourse(accessToken, courseId);
    }

    function getCourseById(accessToken = "", courseId = null) {
        return new Promise(function (reslove, reject) {
            canvasAO.getCourseById(accessToken, courseId).then(function (result) {
                if (!result.errors) {//result contain no error
                    if (result) {
                        reslove(result);
                    }
                    else {//contain nothing
                        reslove([]);
                    }
                }
                else if (result.errors[0].message == "Invalid access token.") {
                    reject(new errorHelper.AccessTokenNotCorrect(accessToken));
                }
                reject(new errorHelper.InternalError(new Error("internal error")));
            }).catch(function (error) {
                reject(error);
            });
        });
    };

    this.getCourseById = (accessToken = "", courseId = null) => {
        return getCourseById(accessToken, courseId);
    }

    function getGradeColumnByCourseId(accessToken = "", courseId = null) {
        return new Promise(function (reslove, reject) {
            canvasAO.getGradeColumnByCourseId(accessToken, courseId).then(function (result) {
                if (!result.errors) {//result contain no error
                    if (result) {
                        reslove(result);
                    }
                    else {//contain nothing
                        reslove([]);
                    }
                }
                else if (result.errors[0].message == "Invalid access token.") {
                    reject(new errorHelper.AccessTokenNotCorrect(accessToken));
                }
                reject(new errorHelper.InternalError(new Error("internal error")));
            }).catch(function (error) {
                reject(error);
            });
        });
    };

    this.getGradeColumnByCourseId = (accessToken = "", courseId = null) => {
        return getGradeColumnByCourseId(accessToken, courseId);
    }

    function getGradeByCourseIdAndStudentId(accessToken = "", courseId = null, studentId = null) {
        return new Promise(function (reslove, reject) {
            if (studentId) {
                let studentIdArrary;
                try {
                    studentIdArrary = JSON.parse(studentId);
                } catch (error) {
                    reject(new errorHelper.InputError(new Error(), [studentId]));
                }
                canvasAO.getGradeByCourseIdAndStudentId(accessToken, courseId, studentIdArrary).then(function (result) {
                    if (!result.errors) {//result contain no error
                        if (result) {
                            reslove(result);
                        }
                        else {//contain nothing
                            reslove([]);
                        }
                    }
                    else if (result.errors[0].message == "Invalid access token.") {
                        reject(new errorHelper.AccessTokenNotCorrect(accessToken));
                    }
                    reject(new errorHelper.InternalError(new Error("internal error")));
                }).catch(function (error) {
                    reject(error);
                });
            } else {
                reject(new errorHelper.QueryParameterIsMissing(new Error(), [studentId]));
            }
        });
    };

    this.getGradeByCourseIdAndStudentId = (accessToken = "", courseId = null, studentId = null) => {
        return getGradeByCourseIdAndStudentId(accessToken, courseId, studentId);
    }

    function postConversation(accessToken = "", courseId = null, studentId = null, subject = "", body = "") {
        return new Promise(function (reslove, reject) {
            if (studentId && subject && body) {
                let studentIdArrary;
                try {
                    studentIdArrary = JSON.parse(studentId);
                } catch (error) {
                    reject(new errorHelper.InputError(new Error(), [studentId]));
                }
                canvasAO.postConversation(accessToken, courseId, studentIdArrary, subject, body).then(function (result) {
                    if (!result.errors) {//result contain no error
                        if (result) {
                            reslove(result);
                        }
                        else {//contain nothing
                            reslove([]);
                        }
                    }
                    else if (result.errors[0].message == "Invalid access token.") {
                        reject(new errorHelper.AccessTokenNotCorrect(accessToken));
                    }
                    reject(new errorHelper.InternalError(new Error("internal error")));
                }).catch(function (error) {
                    reject(error);
                });
            } else {
                reject(new errorHelper.QueryParameterIsMissing(new Error(), [studentId, subject, body]));
            }
        });
    };

    this.postConversation = (accessToken = "", courseId = null, studentId = null, subject = "", body = "") => {
        return postConversation(accessToken, courseId, studentId, subject, body);
    }
}