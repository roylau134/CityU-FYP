"use strict";
const CanvasObjectController = require("./canvasObjectController.js");
const responseHelper = require("../response/responseHelper.js");
const errorHelper = require("../helper/errorHelper.js");
const AuthAO = require("../accessObject/authAO.js");
const WeDbAO = require("../accessObject/webDbAO.js");
const zxcvbn = require('zxcvbn');
const jwtDecode = require('jwt-decode');
const Entities = require('html-entities').XmlEntities; 
const entities = new Entities();
const querystring = require('querystring');

module.exports = function () {
    const canvasObjectController = new CanvasObjectController();
    const authAO = new AuthAO();
    const weDbAO = new WeDbAO();

    function getEmail(accessToken = "") {
        accessToken = accessToken.trim();
        return new Promise(function (reslove, reject) {
            canvasObjectController.getEmail(accessToken).then(function (result) {
                if (result) {
                    const data = {
                        email: result
                    };
                    reslove(responseHelper.commonResponse(data));
                }
                else {
                    reslove(responseHelper.commonResponse({}, "Your Canvas acount does not contain any email address", 102, true));
                }
            }).catch(function (error) {
                if (error instanceof errorHelper.AccessTokenNotCorrect) {
                    reslove(responseHelper.commonResponse({}, error.message, 101, true));
                } else {
                    reslove(responseHelper.commonResponse({}, "Internal error", 100, true));
                }
            });
        });
    }

    this.getEmail = (accessToken) => {
        return getEmail(accessToken);
    }

    function signUpForNewUser(email = "", accessToken = "", password = "") {
        email = email.trim();
        accessToken = accessToken.trim();
        password = password.trim();
        return new Promise(function (reslove, reject) {
            if (zxcvbn(password).score >= 3) {
                canvasObjectController.getEmail(accessToken).then(function (emailByAccessToken) {
                    if (email === emailByAccessToken) {
                        authAO.signUp(emailByAccessToken, password).then(function (result) {
                            const data = {
                                "authToken": result.id_token
                            };
                            weDbAO.insertUser(email, accessToken).then(function (isInsertSuccess) {
                                if (isInsertSuccess) {
                                    reslove(responseHelper.commonResponse(data));
                                } else {
                                    reslove(responseHelper.commonResponse({}, "unable to store the user info to web DB", 105, true));
                                }
                            }).catch(function (error) {
                                reslove(responseHelper.commonResponse(error.data, "Error happened, please refer to the data", 105, true));
                            });
                        }).catch(function (error) {
                            if (error instanceof errorHelper.InputError && error.data[0] && error.data[0].field && error.data[0].field === "username" && error.data[0].message === "TAKEN") {
                                reslove(responseHelper.commonResponse({}, "This email is registerd, please login or try an other email", 104, true));
                            }
                            else {
                                reslove(responseHelper.commonResponse(error.data, "Error happened, please refer to the data", 105, true));
                            }
                        });
                    } else {
                        reslove(responseHelper.commonResponse({}, "Your Canvas acount does not contain any email address", 102, true));
                    }
                })
            } else {
                reslove(responseHelper.commonResponse({}, "Your password is not strong enough", 103, true));
            }
        })
    }

    this.signUpForNewUser = (email, accessToken, password) => {
        return signUpForNewUser(email, accessToken, password);
    }

    function getUserInfo(jwt = "") {
        return new Promise(function (reslove, reject) {
            jwtToUserAccessToken(jwt).then(function (accessToken) {
                canvasObjectController.getUserName(accessToken).then(function (userName) {
                    const result = { userName };
                    reslove(responseHelper.commonResponse(result));
                }).catch(function (error) {
                    console.log(error);
                    reslove(responseHelper.commonResponse({}, "Internal error", 104, true));
                });
            }).catch(function (errorRespones) {
                reslove(errorRespones);
            });
        });
    };

    this.getUserInfo = (jwt = "") => {
        return getUserInfo(jwt);
    };

    function getCourse(jwt = "") {
        return new Promise(function (reslove, reject) {
            jwtToUserAccessToken(jwt).then(function (accessToken) {
                canvasObjectController.getCourse(accessToken).then(function (course) {
                    const result = { course };
                    reslove(responseHelper.commonResponse(result));
                }).catch(function (error) {
                    console.log(error);
                    reslove(responseHelper.commonResponse({}, "Internal error", 104, true));
                });
            }).catch(function (errorRespones) {
                reslove(errorRespones);
            });
        });
    };

    this.getCourse = (jwt = "") => {
        return getCourse(jwt);
    };

    function getUserOfCourse(jwt = "", courseId = null) {
        return new Promise(function (reslove, reject) {
            jwtToUserAccessToken(jwt).then(function (accessToken) {
                canvasObjectController.getUserOfCourse(accessToken, courseId).then(function (course) {
                    const result = { course };
                    reslove(responseHelper.commonResponse(result));
                }).catch(function (error) {
                    console.log(error);
                    reslove(responseHelper.commonResponse({}, "Internal error", 104, true));
                });
            }).catch(function (errorRespones) {
                reslove(errorRespones);
            });
        });
    };

    this.getUserOfCourse = (jwt = "", courseId = null) => {
        return getUserOfCourse(jwt, courseId);
    };

    function getCourseById(jwt = "", courseId = null) {
        return new Promise(function (reslove, reject) {
            jwtToUserAccessToken(jwt).then(function (accessToken) {
                canvasObjectController.getCourseById(accessToken, courseId).then(function (course) {
                    const result = { course };
                    reslove(responseHelper.commonResponse(result));
                }).catch(function (error) {
                    console.log(error);
                    reslove(responseHelper.commonResponse({}, "Internal error", 104, true));
                });
            }).catch(function (errorRespones) {
                reslove(errorRespones);
            });
        });
    };

    this.getCourseById = (jwt = "", courseId = null) => {
        return getCourseById(jwt, courseId);
    };

    function getGradeColumnByCourseId(jwt = "", courseId = null) {
        return new Promise(function (reslove, reject) {
            jwtToUserAccessToken(jwt).then(function (accessToken) {
                canvasObjectController.getGradeColumnByCourseId(accessToken, courseId).then(function (gradeColumn) {
                    const result = { gradeColumn };
                    reslove(responseHelper.commonResponse(result));
                }).catch(function (error) {
                    console.log(error);
                    reslove(responseHelper.commonResponse({}, "Internal error", 104, true));
                });
            }).catch(function (errorRespones) {
                reslove(errorRespones);
            });
        });
    };

    this.getGradeColumnByCourseId = (jwt = "", courseId = null) => {
        return getGradeColumnByCourseId(jwt, courseId);
    };

    function getGradeByCourseIdAndStudentId(jwt = "", courseId = null, studentId = null) {
        return new Promise(function (reslove, reject) {
            jwtToUserAccessToken(jwt).then(function (accessToken) {
                canvasObjectController.getGradeByCourseIdAndStudentId(accessToken, courseId, entities.decode(studentId)).then(function (grade) {
                    const result = { grade };
                    reslove(responseHelper.commonResponse(result));
                }).catch(function (error) {
                    console.log(error);
                    reslove(responseHelper.commonResponse({}, "Internal error", 104, true));
                });
            }).catch(function (errorRespones) {
                reslove(errorRespones);
            });
        });
    };

    this.getGradeByCourseIdAndStudentId = (jwt = "", courseId = null, studentId = null) => {
        return getGradeByCourseIdAndStudentId(jwt, courseId, studentId);
    };

    function postConversation(jwt = "", courseId = null, studentId = null, subject = "", body = "") {
        return new Promise(function (reslove, reject) {
            jwtToUserAccessToken(jwt).then(function (accessToken) {
                canvasObjectController.postConversation(accessToken, courseId, entities.decode(studentId), querystring.escape(subject), querystring.escape(body)).then(function (response) {
                    const result = { response };
                    reslove(responseHelper.commonResponse(result));
                }).catch(function (error) {
                    console.log(error);
                    reslove(responseHelper.commonResponse({}, "Internal error", 104, true));
                });
            }).catch(function (errorRespones) {
                reslove(errorRespones);
            });
        });
    };

    this.postConversation = (jwt = "", courseId = null, studentId = null, subject = "", body = "") => {
        return postConversation(jwt, courseId, studentId, subject, body);
    };

    function jwtToUserAccessToken(jwt = "") {
        jwt = jwt.trim();
        return new Promise(function (reslove, reject) {
            if (jwt) {
                let jwtJson;
                try {
                    jwtJson = jwtDecode(jwt);
                }
                catch (errro) {
                    reject(responseHelper.commonResponse({}, "jwt is not correct", 202, true));
                }
                const userId = jwtJson.sub;
                authAO.getAccountInfo(userId).then(function (userInfo) {
                    const email = userInfo.email;
                    weDbAO.getUserAccessToken(email).then(function (accessToken) {
                        reslove(accessToken);
                    }).catch(function (error) {
                        console.log(error);
                        reject(responseHelper.commonResponse({}, "Internal error", 201, true));
                    });
                }).catch(function (error) {
                    console.log(error);
                    reject(responseHelper.commonResponse({}, "Internal error", 200, true));
                });
            } else {
                reject(responseHelper.commonResponse({}, "jwt is missing", 203, true));
            }
        });
    };
}