"use strict";

function commonResponse(data = {}, message = "", messageCode = null, isError = false) {
    const respose = {
        "data": data,
        "message": message,
        "messageCode": messageCode,
        "isError": isError
    };
    return respose;
}

function authErrorResponse() {
    return commonResponse({}, "Api-key is not correct", 404, true);
}

module.exports = {
    commonResponse,
    authErrorResponse
};