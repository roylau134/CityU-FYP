const WebDbHelper = require("../helper/webDbHelper.js");
const errorHelper = require('../helper/errorHelper.js');

module.exports = function () {
    const webDbHelper = new WebDbHelper();

    function insertUser(emailAddress = "", canvasAccessToken = "") {
        return new Promise(function (reslove, reject) {
            if (emailAddress && canvasAccessToken) {
                const query = "INSERT INTO `userInfo`(`email`, `canvasAccessToken`) VALUES (?, ?)";
                const queryData = [emailAddress, canvasAccessToken];
                webDbHelper.query(query, queryData).then(function (result) {
                    if (result.insertId) {
                        reslove(true);
                    }
                    else {
                        reject(new errorHelper.DatabaseError(new Error()));
                    }
                }).catch(function (error) {
                    reject(new errorHelper.DatabaseError(errro));
                });
            }
            else {
                const queryDataName = ["emailAddress", "canvasAccessToken"];
                reject(new errorHelper.QueryParameterIsMissing(new Error, queryDataName));
            }
        });
    }

    this.insertUser = (emailAddress, canvasAccessToken) => {
        return insertUser(emailAddress, canvasAccessToken)
    };

    function getUserInfo(emailAddress = "") {
        return new Promise(function (reslove, reject) {
            if (emailAddress) {
                const query = "SELECT * FROM `userInfo` WHERE `email` = ?"; //canvasAccessToken
                const queryData = [emailAddress];
                webDbHelper.query(query, queryData).then(function (result) {
                    if (result && result[0]) {
                        reslove(result[0]);
                    }
                    else {
                        reject(new errorHelper.DatabaseError(new Error()));
                    }
                }).catch(function (error) {
                    reject(new errorHelper.DatabaseError(errro));
                });
            }
            else {
                const queryDataName = ["emailAddress", "canvasAccessToken"];
                reject(new errorHelper.QueryParameterIsMissing(new Error, queryDataName));
            }
        });
    }

    this.getUserInfo = (emailAddress) => {
        return getUserInfo(emailAddress)
    };

    function getUserAccessToken(emailAddress = "") {
        return new Promise(function (reslove, reject) {
            getUserInfo(emailAddress).then(function (dbResult) {
                if (dbResult && dbResult.canvasAccessToken) {
                    reslove(dbResult.canvasAccessToken);
                } else {
                    reject(new errorHelper.DatabaseDataNotFopund(new Error));
                }
            }).catch(function (error) {
                reject(error);
            })
        });
    };

    this.getUserAccessToken = (emailAddress = "") => {
        return getUserAccessToken(emailAddress);
    }
}