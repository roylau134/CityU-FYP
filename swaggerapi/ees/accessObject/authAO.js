"use strict";
const errorHelper = require("../helper/errorHelper.js");
const ApiCallHelper = require("../helper/apiCallHelper.js");
const apiPathJson = require("../static/authApiPath.json");

module.exports = function () {
    const apiCallHelper = new ApiCallHelper();
    const baseAuth = apiPathJson.basicAuth;
    const baseDomain = apiPathJson.authHost;
    const basePort = apiPathJson.port;

    function signUp(emailByAccessToken = "", password = "") {
        return new Promise(function (reslove, reject) {
            if (emailByAccessToken && password) {
                const configJson = apiPathJson.signUp;
                const path = `${configJson.path}?username=${emailByAccessToken}&password=${password}`;
                const requesOption = getOption(path, configJson.method);
                apiCallHelper.httpApiCall(requesOption).then(function (resultJSON) {
                    if (resultJSON.result) {
                        reslove(resultJSON.result);
                    }
                    else {
                        reject(new errorHelper.InputError(new Error(), resultJSON.errors));
                    }
                });
            } else {
                reject(new errorHelper.InputIsMissing());
            }
        });
    }

    function getOption(path, method, isNeedAuth = false) {
        const header = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true',
            'Origin': 'http://localhost'
        };
        const option = apiCallHelper.apiOption(baseDomain, path, basePort, method, header)
        option.secureProtocol = 'SSLv3_method';
        if (isNeedAuth) {
            option.auth = `${baseAuth}`;
        }
        return option;
    }

    this.signUp = (emailByAccessToken = "", password = "") => {
        return signUp(emailByAccessToken, password);
    }

    function getAccountInfo(id = "") {
        return new Promise(function (reslove, reject) {
            if (id) {
                const configJson = apiPathJson.accounts;
                const path = `${configJson.path}/${id}`; //for private API need to add baseAuth
                const requesOption = getOption(path, configJson.method, true);
                apiCallHelper.httpApiCall(requesOption).then(function (resultJSON) {
                    if (resultJSON.result) {
                        const result = {
                            email: resultJSON.result.username,
                            id: resultJSON.result.id
                        };
                        reslove(result);
                    }
                    else {
                        reject(new errorHelper.InputError(new Error(), resultJSON.errors));
                    }
                });
            } else {
                reject(new errorHelper.InputIsMissing());
            }
        });
    };

    this.getAccountInfo = (id = "") => {
        return getAccountInfo(id);
    }
}
