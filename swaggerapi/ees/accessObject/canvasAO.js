"use strict";
const ApiCallHelper = require("../helper/apiCallHelper.js");
const CheckHelp = require("../helper/checkHelper.js");
const apiPathJson = require("../static/canvasApiPath.json");
const errorHelper = require('../helper/errorHelper.js');
const util = require('util');

module.exports = function () {
    const apiCallHelper = new ApiCallHelper();
    const checkHelp = new CheckHelp();
    const baseDomain = apiPathJson.canvasHost;
    const basePort = null;
    const authHeader = apiPathJson.authHeader.headerName;
    const authPrefix = apiPathJson.authHeader.valuePrefix;

    function getUserInfo(accessToken = "") {
        if (!isAccessTokenPass(accessToken)) {
            throw (new errorHelper.AccessTokenNotCorrect(accessToken));
        }
        const configJson = apiPathJson.getUserInfo;
        const requesOption = getOption(configJson.path, configJson.method, accessToken);
        return apiCallHelper.httpsApiCall(requesOption);
    }

    function getOption(path, method, auth = "") {
        let header = {};
        if (auth) {
            header[authHeader] = authPrefix + auth;
        }
        return apiCallHelper.apiOption(baseDomain, path, basePort, method, header);
    }

    function isAccessTokenPass(accessToken = null) {
        return accessToken && checkHelp.isForAccessToken(accessToken);
    }

    this.getUserInfo = (accessToken) => {
        return getUserInfo(accessToken);
    }

    function getCoursePaged(accessToken = "", page = 1, perPage = 5) {
        if (!isAccessTokenPass(accessToken)) {
            throw (new errorHelper.AccessTokenNotCorrect(accessToken));
        }
        const configJson = apiPathJson.getCourse;
        const path = `${configJson.path}?page=${page}&per_page=${perPage}&include[]=term&include[]=teachers`;
        const requesOption = getOption(path, configJson.method, accessToken);
        return apiCallHelper.httpsApiCall(requesOption);
    }

    function getAllCourse(accessToken = "", startPage = 1, perPage = 5, result = [], callingPerTime = 3) {
        return new Promise((reslove, reject) => {
            const promissArrary = [];
            for (let counter = 0; counter < callingPerTime; counter++) {
                const eachJob = new Promise((eachJobReslove, eachJobReject) => {
                    getCoursePaged(accessToken, startPage, perPage).then(function (tempResult) {
                        tempResult = tempResult.filter(item => item.name);//remove empty data (access_restricted_by_date)
                        eachJobReslove(tempResult);
                    }).catch(function (error) {
                        eachJobReject(error)
                    });
                });
                promissArrary.push(eachJob);
                startPage++;
            }

            Promise.all(promissArrary).then(promissResult => {
                let isNotFullOrEmpty = false;
                for (const eachResultIndex in promissResult) {
                    const eachResult = promissResult[eachResultIndex];
                    if (eachResult && eachResult.length > 0) {
                        result = result.concat(eachResult);
                    } else if (!eachResult || eachResult.length == 0) {
                        isNotFullOrEmpty = true;
                    }
                }
                if (isNotFullOrEmpty) {
                    reslove(result);
                } else {
                    reslove(getAllCourse(accessToken, startPage, perPage, result, callingPerTime));
                }
            }).catch(err => {
                reject(err);
            });
        });
    }

    function getCourse(accessToken = "") {
        if (!isAccessTokenPass(accessToken)) {
            throw (new errorHelper.AccessTokenNotCorrect(accessToken));
        }
        const startPage = 1;
        const perPage = 4;
        const result = [];
        const callingPerTime = 4;
        return getAllCourse(accessToken, startPage, perPage, result, callingPerTime);
    }

    this.getCourse = (accessToken) => {
        return getCourse(accessToken);
    }

    function getUserOfCoursePaged(accessToken = "", courseId = null, page = 1, perPage = 5) {
        if (!isAccessTokenPass(accessToken)) {
            throw (new errorHelper.AccessTokenNotCorrect(accessToken));
        } else if (!courseId || !Number.isInteger(courseId)) {
            throw (new errorHelper.InternalError(new Error("courseId is not correct")));
        }
        const configJson = apiPathJson.getUserOfCourse;
        const basePath = util.format(configJson.path, courseId);
        const path = `${basePath}?include[]=avatar_url&include[]=enrollments&include[]=email&include[]=observed_users&include[]=can_be_removed&include[]=custom_links&include_inactive=true&page=${page}&per_page=${perPage}`;
        const requesOption = getOption(path, configJson.method, accessToken);
        return apiCallHelper.httpsApiCall(requesOption);
    }

    function getAllCourseUserOfCourse(accessToken = "", courseId = null, startPage = 1, perPage = 5, result = [], callingPerTime = 5) {
        return new Promise((reslove, reject) => {
            const promissArrary = [];
            for (let counter = 0; counter < callingPerTime; counter++) {
                const eachJob = new Promise((eachJobReslove, eachJobReject) => {
                    getUserOfCoursePaged(accessToken, courseId, startPage, perPage).then(function (tempResult) {
                        tempResult = tempResult.filter(item => item.name);//remove empty data (access_restricted_by_date)
                        eachJobReslove(tempResult);
                    }).catch(function (error) {
                        eachJobReject(error)
                    });
                });
                promissArrary.push(eachJob);
                startPage++;
            }

            Promise.all(promissArrary).then(promissResult => {
                let isNotFullOrEmpty = false;
                for (const eachResultIndex in promissResult) {
                    const eachResult = promissResult[eachResultIndex];
                    if (eachResult && eachResult.length > 0) {
                        result = result.concat(eachResult);
                    } else if (!eachResult || eachResult.length == 0) {
                        isNotFullOrEmpty = true;
                    }
                }
                if (isNotFullOrEmpty) {
                    reslove(result);
                } else {
                    reslove(getAllCourseUserOfCourse(accessToken, courseId, startPage, perPage, result, callingPerTime));
                }
            }).catch(err => {
                reject(err);
            });
        });
    }

    function getUserOfCourse(accessToken = "", courseId = null) {
        courseId = Number.parseInt(courseId);
        if (!isAccessTokenPass(accessToken)) {
            throw (new errorHelper.AccessTokenNotCorrect(accessToken));
        } else if (!courseId || !Number.isInteger(courseId)) {
            throw (new errorHelper.InternalError(new Error("Course Id is not correct")));
        }
        const startPage = 1;
        const perPage = 4;
        const result = [];
        const callingPerTime = 3;
        return getAllCourseUserOfCourse(accessToken, courseId, startPage, perPage, result, callingPerTime);
    }

    this.getUserOfCourse = (accessToken = "", courseId = null) => {
        return getUserOfCourse(accessToken, courseId);
    }

    function getCourseById(accessToken = "", courseId = null) {
        courseId = Number.parseInt(courseId);
        if (!isAccessTokenPass(accessToken)) {
            throw (new errorHelper.AccessTokenNotCorrect(accessToken));
        } else if (!courseId || !Number.isInteger(courseId)) {
            throw (new errorHelper.InternalError(new Error("Course Id is not correct")));
        }
        const configJson = apiPathJson.getCourseById;
        const path = util.format(configJson.path, courseId);
        const requesOption = getOption(path, configJson.method, accessToken);
        return apiCallHelper.httpsApiCall(requesOption);
    }

    this.getCourseById = (accessToken = "", courseId = null) => {
        return getCourseById(accessToken, courseId);
    }

    function getGradeColumnByCourseId(accessToken = "", courseId = null) {
        courseId = Number.parseInt(courseId);
        if (!isAccessTokenPass(accessToken)) {
            throw (new errorHelper.AccessTokenNotCorrect(accessToken));
        } else if (!courseId || !Number.isInteger(courseId)) {
            throw (new errorHelper.InternalError(new Error("Course Id is not correct")));
        }
        const configJson = apiPathJson.gradeColumns;
        const path = util.format(configJson.path, courseId);
        const requesOption = getOption(path, configJson.method, accessToken);
        return apiCallHelper.httpsApiCall(requesOption);
    }

    this.getGradeColumnByCourseId = (accessToken = "", courseId = null) => {
        return getGradeColumnByCourseId(accessToken, courseId);
    }

    function getGradeByCourseIdAndStudentId(accessToken = "", courseId = null, studentIdArray = []) {
        courseId = Number.parseInt(courseId);
        if (!isAccessTokenPass(accessToken)) {
            throw (new errorHelper.AccessTokenNotCorrect(accessToken));
        } else if (!courseId || !Number.isInteger(courseId)) {
            throw (new errorHelper.InternalError(new Error("Course Id is not correct")));
        }
        const configJson = apiPathJson.grade;        
        const path = util.format(configJson.path, courseId, studentIdArray.map((o)=> `student_ids%5B%5D=${o}`).join("&"));
        const requesOption = getOption(path, configJson.method, accessToken);
        return apiCallHelper.httpsApiCall(requesOption);
    }

    this.getGradeByCourseIdAndStudentId = (accessToken = "", courseId = null, studentIdArray = []) => {
        return getGradeByCourseIdAndStudentId(accessToken, courseId, studentIdArray);
    }

    function postConversation(accessToken = "", courseId = null, studentIdArray = [], subject = "", body = "") {
        courseId = Number.parseInt(courseId);
        if (!isAccessTokenPass(accessToken)) {
            throw (new errorHelper.AccessTokenNotCorrect(accessToken));
        } else if (!courseId || !Number.isInteger(courseId)) {
            throw (new errorHelper.InternalError(new Error("Course Id is not correct")));
        }
        const configJson = apiPathJson.conversation;
        const path = util.format(configJson.path, subject, body, courseId, studentIdArray.map((o) => `recipients%5B%5D=${o}`).join("&"));
        const requesOption = getOption(path, configJson.method, accessToken);
        return apiCallHelper.httpsApiCall(requesOption);
    }

    this.postConversation = (accessToken = "", courseId = null, studentIdArray = [], subject = "", body = "") => {
        return postConversation(accessToken, courseId, studentIdArray, subject, body);
    }
}