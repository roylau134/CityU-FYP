const config = require("./ees/static/webDbConfig.json");
// Update with your config settings.

module.exports = {
    client: 'mysql',
    version: '5.7',
    connection: {
        host: 'mysqlForWeb',
        user: config.user,
        password: config.password,
        database: config.database
    },
    pool: {
        min: 2,
        max: 10
    },
    migrations: {
        tableName: 'migrationsInfo'
    },
    debug: true
};
