# Final Year Project - Technology for Enhancing Teaching and Learning (TETL)
> # Basic Info
> - University: City University of Hong Kong
> - Student: Lau Chi Chun ROy
> - Email: ROYLAU134@GMAIL.COM

# Software Require
 - Docker (v17.12) 
 - Docker Compose
  
# Installation Guide
Please refer to the [docker/readme.md](https://gitlab.com/roylau134/CityU-FYP/blob/master/docker/readme.md)

# CICD set up
Please refer to the [/cicdSetup.md](https://gitlab.com/roylau134/CityU-FYP/blob/master/cicdSetup.md)

# Production Tech used
## SSL cert
[sslforfree](https://www.sslforfree.com/)
## DNS
[Freenom](https://my.freenom.com/domains.php)
## Server
[Digital Ocean](https://cloud.digitalocean.com/)

# Start the web
## Web app UI
https://www.tetl.ml/

## Web app Swagger ui
https://www.tetl.ml/swagger/ui

## Web app Swagger api
https://www.tetl.ml/api/

## Auth api ui
https://www.tetl.ml/api/auth/

## PhpMyAdmin - web database
http://authadmin.tetl.ml/

## PhpMyAdmin - Auth database
http://webadmin.tetl.ml/