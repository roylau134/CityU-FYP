// Poll for jQuery to come into existance
var checkReady = function (callback) {
    if (window.jQuery) {
        callback(jQuery);
    }
    else {
        window.setTimeout(function () { checkReady(callback); }, 20);
    }
};

var checkZxcvbnReady = function (callback) {
    checkReady(function ($) {
        if (window.jQuery().zxcvbnProgressBar) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function () { checkZxcvbnReady(callback); }, 20);
        }
    });
};

var checkDialogReady = function (callback) {
    checkReady(function ($) {
        if (document.getElementById("loginDialog")) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function () { checkDialogReady(callback); }, 20);
        }
    });
};

var checkKeratinReady = function (callback) {
    checkReady(function ($) {
        if (typeof KeratinAuthN !== "undefined") {
            callback(KeratinAuthN);
        }
        else {
            window.setTimeout(function () { checkKeratinReady(callback); }, 20);
        }
    });
};

var checkLoader = function (callback) {
    if (document.getElementById("main").style.display == "block") {
        callback(null);
    }
    else {
        window.setTimeout(function () { checkLoader(callback); }, 20);
    }
};

var checkIsLogined = function (callback) {
    if (typeof isLogined !== 'undefined') {
        isLogined(function (isLoginedBool) {
            callback(isLoginedBool);
        });
    }
    else {
        window.setTimeout(function () { checkIsLogined(callback); }, 20);
    }
};

var isLogined = (callback) => {
    checkKeratinReady(function ($) {
        window.setTimeout(function () { callback(KeratinAuthN.session() !== undefined); }, 300);
    });
}

var checkToastIsReady = (callback) => {
    checkReady(() => {
        if (typeof toastr !== 'undefined') {
            callback();
        }
        else {
            window.setTimeout(() => {
                checkToastIsReady(callback);
            }, 20);
        }
    });
};

//init toastr
checkToastIsReady(() => {
    window.setTimeout(() => {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    }, 500);
})

loadJsScript();
loadCss();

function loadJsScript() {
    const jQuery = "https://code.jquery.com/jquery-3.3.1.min.js";
    loadAsScript(jQuery);

    const loadJsList = ["https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js", "https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/1.0/zxcvbn.min.js", "https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js", "/content/javascript/loader.js", "/content/javascript/zxcvbn-bootstrap-strength-meter.js", "/content/javascript/api.js", "/content/javascript/keratin-authn.min.js", "/content/javascript/keratin.js",
        "https://codeseven.github.io/toastr/build/toastr.min.js"];

    for (const key in loadJsList) {
        checkReady(function ($) {
            loadAsScript(loadJsList[key]);
        });
    }
}

function loadAsScript(url) {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.src = url;
    head.appendChild(script);
}

function loadCss() {
    const loadCssJson = {
        "bootstrapCss": "https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css",
        "common": "/content/css/common.css",
        "toastr": "https://codeseven.github.io/toastr/build/toastr.min.css"
    }

    for (const key in loadCssJson) {
        loadAsCss(key, loadCssJson[key]);
    }
}

function loadAsCss(cssId, url) {
    if (!document.getElementById(cssId)) {
        var head = document.getElementsByTagName('head')[0];
        var link = document.createElement('link');
        link.id = cssId;
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = url;
        link.media = 'all';
        head.appendChild(link);
    }
}