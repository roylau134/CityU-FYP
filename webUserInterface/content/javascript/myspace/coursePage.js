var checkDataTable = function (callback) {
    checkReady(() => {
        if (typeof $().DataTable !== 'undefined') {
            callback();
        }
        else {
            window.setTimeout(() => {
                checkDataTable(callback);
            }, 20);
        }
    });
};

var checkDataTableExtenstion = function (callback) {
    checkDataTable(() => {
        if (typeof $().DataTable().select !== 'undefined' && typeof $().DataTable().colReorder !== 'undefined' && typeof $().DataTable().buttons !== 'undefined') {
            callback();
        }
        else {
            window.setTimeout(() => {
                checkDataTableExtenstion(callback);
            }, 20);
        }
    });
};

var checkDataIsReady = (callback) => {
    if (userData && userData.length > 0 && gardeSelectionArrary) {
        callback();
    }
    else {
        window.setTimeout(() => {
            checkDataIsReady(callback);
        }, 20);
    }
};

const courseId = getCourseIdFromURL();
const courseInfo = setCourseInfo();
let courseNameStore;

function getCourseIdFromURL() {
    const path = window.location.pathname;
    const regexPattern = /^.+-([\d]+)$/;
    const courseId = path.match(regexPattern)[1];
    return courseId;
}

let tempUserData;
let userData;
let gardeSelectionArrary;
let gradeSelected = "";
let courseTable;
const commonColumnBase = 2;

function getUserOfCoursePrettify(jwt, courseId, callback) {
    getUserOfCourse(jwt, courseId, (userDataResult) => {
        const courseData = userDataResult.data.course;
        const tempUserData = {};
        courseData.map((o) => {
            tempUserData[o.id] = [o.id, o.sortable_name, o.enrollments[0].role.match(/^([\w]+)Enrollment$/)[1]];
        }
        );
        const studentId = courseData.filter((o) => o.enrollments[0].role.match(/^([\w]+)Enrollment$/)[1] != "Teacher").map((o) => o.id);
        const otherId = courseData.filter((o) => studentId.indexOf(o.id) < 0).map((o) => o.id);
        for (index in otherId) {
            for (garde in gardeSelectionArrary) {
                tempUserData[otherId[index]] = tempUserData[otherId[index]].concat("N/A");
            }
        }
        getGrade(jwt, courseId, encodeURIComponent(JSON.stringify(studentId)), (grade) => {
            const gradeData = grade.data.grade;
            for (key in gradeData) {
                const student = gradeData[key];
                let tempUser = tempUserData[student.user_id];
                for (gradeKey in gardeSelectionArrary) {
                    const eachGrade = student.submissions.filter((o) => o.assignment_id == gradeKey);
                    if (eachGrade.length != 0) {
                        tempUser = tempUser.concat(`${eachGrade[0].grade} (${eachGrade[0].score})`);
                    } else {
                        tempUser = tempUser.concat("N/A");
                    }
                }
                tempUserData[student.user_id] = tempUser;
            }
            let result = [];
            for (key in tempUserData) {
                result = result.concat([tempUserData[key]]);
            }
            callback(result);
        });
    })
}

function getGradeColumnPrettify(jwt, courseId, callback) {
    getGradeColumn(jwt, courseId, (gardeSelectionArrary) => {
        let result = {};
        const filteredResultArrary = gardeSelectionArrary.data.gradeColumn.filter(column => column.assignments.length != 0);
        for (const key in filteredResultArrary) {
            const filteredResult = filteredResultArrary[key];
            for (const assignmentKey in filteredResult.assignments) {
                const assignment = filteredResult.assignments[assignmentKey];
                result[assignment.id] = assignment;
            }
        }
        callback(result);
    })
}

function setCourseInfo() {
    isLogined(function () {
        KeratinAuthN.restoreSession().then(function () {
            const jwt = KeratinAuthN.session();
            const courseIdTemp = courseId;
            getCourseById(jwt, courseIdTemp, (result) => {
                updateCourseName(result.data.course.name);
                return result.data.course;
            });
        });
    });

    function updateCourseName(courseName) {
        courseNameStore = courseName;
        $("#courseName").html(courseName);
    }
}

function changeGrageSelection(event, element) {
    event.preventDefault();
    const selectionArear = $("#selectionArear");
    const id = element.val();
    if (id) {
        selectionArear.removeClass("d-none");
        gradeSelected = gardeSelectionArrary[id];
        let columnBase = commonColumnBase;
        for (key in gardeSelectionArrary) {
            columnBase++;
            if (id != key) {
                courseTable.columns([columnBase]).visible(false, false);
            } else {
                courseTable.columns([columnBase]).visible(true, true);
            }
        }
    } else {
        selectionArear.addClass("d-none");
        courseTable.columns().visible(true, true);
        courseTable.rows().deselect()
        $("#searchValue").val("");
    }
}

function selectTheStudent() {
    const valueRange = $("#valueRange").val();
    const searchValue = $("#searchValue").val();
    const valueType = $("#valueType").val();
    const gradeSelection = $("#gradeSelection").val();
    let base = commonColumnBase;
    for (key in gardeSelectionArrary) {
        base++;
        if (key == gradeSelection) {
            courseTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
                const data = this.data();
                const valueString = new String(data[base]);
                const regexPattern = /^[^\s]+ \((\d+)\)$/;
                const match = valueString.match(regexPattern);
                if (match) {
                    const value = regexPattern.exec(valueString)[1];
                    if (isNeedToSelect(value)) {
                        this.select();
                    }
                }
            });
            break;
        }
    }

    function isNeedToSelect(value) {
        let compareTo;
        switch (valueType) {
            case "digit":
                compareTo = parseInt(value);
                break;
            case "percent":
                const maxPoint = gardeSelectionArrary[gradeSelection].points_possible;
                compareTo = parseInt(value / maxPoint * 100);
                break;
            default:
                return false;
        }
        const target = parseInt(searchValue);
        switch (valueRange) {
            case "lessThan":
                return compareTo < target;
            case "lessThanOrEqual":
                return compareTo <= target;
            case "equal":
                return compareTo == target;
            case "largeerThan":
                return compareTo > target;
            case "largeerThanOrEqual":
                return compareTo >= target;
            default:
                return false;
        }
    }
}

checkReady(() => {
    loadAsScript("//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js");
    loadAsCss("datatables", "//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css");
    checkDataTable(() => {
        loadAsScript("//cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js");
        loadAsScript("//cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js");
        loadAsScript("//cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js");
        checkDataTableExtenstion(() => {
            checkDataIsReady(() => {
                let columns = [
                    { title: "ID" },
                    { title: "Name" },
                    { title: "Type" }];
                for (const key in gardeSelectionArrary) {
                    const gardeSelection = gardeSelectionArrary[key];
                    columns = columns.concat({ title: gardeSelection.name });
                }
                for (const index in userData) {
                    for (const key in gardeSelectionArrary) {
                        userData[index] = userData[index].concat(0);
                    }
                }
                courseTable = $("#courseTable").DataTable({
                    data: userData,
                    columns: columns,
                    select: true,
                    colReorder: true,
                    responsive: true,
                    dom: 'Slfrtip'
                });
            })
        })
    });
    isLogined(function () {
        KeratinAuthN.restoreSession().then(function () {
            const jwt = KeratinAuthN.session();
            getUserOfCoursePrettify(jwt, courseId, (userDataResult) => {
                userData = userDataResult;
            })
            getGradeColumnPrettify(jwt, courseId, (gardeSelectionArraryResult) => {
                const gradeSelectionDropdownElement = $("#gradeSelection");
                const gradeSelectionElement = $("<option></option>");
                gardeSelectionArrary = gardeSelectionArraryResult;
                for (const key in gardeSelectionArrary) {
                    const gardeSelection = gardeSelectionArrary[key];
                    const eachElement = gradeSelectionElement.clone();
                    eachElement.html(gardeSelection.name);
                    eachElement.val(key);
                    gradeSelectionDropdownElement.append(eachElement);
                }
                gradeSelectionDropdownElement.change((event) => {
                    changeGrageSelection(event, gradeSelectionDropdownElement);
                })
            })
            $("#select").click(() => {
                selectTheStudent();
            });
            $("#popUpButton").click(() => {
                $("#subject").val(`${courseId} ${courseNameStore} - `);
                $("#body").val("");
            });
            $("#sendMessage").click((event) => {
                $("#conPopUp").modal('hide');
                const jwt = KeratinAuthN.session();
                const subject = $("#subject").val();
                const body = $("#body").val();
                let student = [];
                const temp = courseTable.rows({ selected: true }).data();
                for (let index = 0; index < temp.length; index++) {
                    student = student.concat(temp[index][0]);
                }
                postConversation(jwt, courseId, encodeURIComponent(JSON.stringify(student)), encodeURIComponent(subject), encodeURIComponent(body), (response) => {
                    console.log(JSON.stringify(response));
                    if (response && !response.isError) {
                        toastr["success"]("Message has been sent");
                    } else {
                        toastr["warning"]("Unable to send message");
                    }
                })
            })
        });
    });
});