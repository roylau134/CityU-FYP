checkIsLogined(function (isLoginedBool) {
    if (!isLoginedBool) {
        toastr["error"]("You need to login first!");
        focusToLogin();
    } else {
        //TODO: display course card
        checkDispalyCourseCardReady(function () {
            dispalyCourseCard();
        });
    }
});

var checkDispalyCourseCardReady = function (callback) {
    if (typeof dispalyCourseCard !== 'undefined') {
        callback();
    }
    else {
        window.setTimeout(function () { checkDispalyCourseCardReady(callback); }, 20);
    }
};