let courseCardTemplate;

checkReady(function () {
    $.get("/myspace/courseCardTemplate.html", function (result) {
        courseCardTemplate = $(result);
    });
});

var checkCourseCardTemplateReady = function (callback) {
    checkReady($ => {
        if (courseCardTemplate) {
            callback(courseCardTemplate);
        }
        else {
            window.setTimeout(() => { checkCourseCardTemplateReady(callback); }, 20);
        }
    });
};


function dispalyCourseCard() {
    this.jwt = KeratinAuthN.session();
    if (this.jwt) {
        getCourse(this.jwt, result => {
            this.courses = result.data.course.sort(
                (a, b) => {
                    a = new Date(a.start_at);
                    b = new Date(b.start_at);
                    return a > b ? -1 : a < b ? 1 : 0;
                });
            console.log(this.courses)
            checkCourseCardTemplateReady(courseCardTemplate => {
                console.log(courseCardTemplate);
                const courseDisplayCardDeck = $("#courseDisplayCardDeck");
                for (const index in this.courses) {
                    const courseCard = courseCardTemplate.clone();

                    const coureInfo = this.courses[index];

                    const courseLink = `/myspace/course/${coureInfo.name.replace(/[^\w\s-]/g, "").replace(/[\s]+/g, " ").replace(/ /g, "-")}-${coureInfo.id}`;
                    courseCard.find(".courseName").html(coureInfo.name);
                    courseCard.find(".courseName").attr("href", courseLink);

                    const canvasLink = `https://canvas.cityu.edu.hk/courses/${coureInfo.id}`;
                    courseCard.find(".canvasLink").attr("href", canvasLink);

                    const pHtml = $(document.createElement("p")).addClass("p-0 m-0");
                    courseCard.find(".content").append(pHtml.clone().html(`${coureInfo.term.name}`));
                    courseCard.find(".content").append(pHtml.clone().html(`Course Code: ${coureInfo.course_code}`));

                    let isFirstTeacher = true;
                    const teacherLinkTemplate = $(document.createElement("a"));
                    const teacherPHtml = pHtml.clone().append("Teacher: ");
                    for (const teacherIndex in coureInfo.teachers) {
                        const teacherInfo = coureInfo.teachers[teacherIndex];
                        const teacherLinkURL = `https://canvas.cityu.edu.hk/courses/${coureInfo.id}/users/${teacherInfo.id}`;
                        const teacherLink = teacherLinkTemplate.clone().html(teacherInfo.display_name).attr("href", teacherLinkURL);
                        if (!isFirstTeacher) {
                            teacherPHtml.append(", ");
                        }
                        teacherPHtml.append(teacherLink);
                        isFirstTeacher = false;
                    }
                    courseCard.find(".content").append(teacherPHtml);

                    courseDisplayCardDeck.append(courseCard);
                }
            });
        });
    }
}

