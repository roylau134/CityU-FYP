checkReady(function () {
    $(document).ready(function () {
        const divOfBody = $("body");

        initMemberInfo();

        function initMemberInfo() {
            isLogined(function (isLoginedBool) {
                if (!isLoginedBool) {
                    //add popup dialog
                    $.get("/myspace/logindialog", function (result) {
                        divOfBody.append(result);

                        bindSignUp($("#dialogSignUp"));
                    });

                    //add non-member info to nav
                    $.get("/myspace/nonmemberinfo", function (result) {
                        const memberInfoElement = $("#memberInfo");
                        memberInfoElement.append(result);

                        //map button to same action
                        bindLoginDialog($("#navLogin"));
                        bindSignUp($("#navSignUp"));
                        addLoginJs();
                    });
                } else {
                    checkKeratinReady(function () {
                        KeratinAuthN.restoreSession().then(function () {
                            const jwt = KeratinAuthN.session();
                            getUserName(jwt, function (dataJson) {
                                if (dataJson && !dataJson.isError && dataJson.data && dataJson.data.userName) {
                                    const userName = dataJson.data.userName;
                                    $("#memberInfo").html(`Hello, ${userName}`);
                                    //TODO: add sign out button
                                    const logoutButton = $("#logout");
                                    logoutButton.removeClass("d-none");
                                    logoutButton.click(() => {
                                        KeratinAuthN.logout().then(() => {
                                            toastr["info"]("Logouted!");
                                            window.setTimeout(() => {
                                                location.reload();
                                            }, 500);
                                        });
                                    });
                                }
                            });
                        });
                    })
                }
            });
        };
    });
});

function addLoginJs() {
    const url = "/content/javascript/login.js";
    checkReady(function ($) {
        loadAsScript(url);
    });
}

function bindLoginDialog(element) {
    const logindialogId = "#loginDialog";
    element.attr("data-target", logindialogId);
}

function bindSignUp(element) {
    const signUpPath = "/myspace/signup"
    element.attr("href", signUpPath);
}

function focusToLogin() {
    checkReady(function () {
        checkLoader(function () {
            checkDialogReady(function () {
                const loginDialog = $("#loginDialog");
                loginDialog.modal('show');
            });
        });
    });
}