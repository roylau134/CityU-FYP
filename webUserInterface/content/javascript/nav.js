checkReady(function ($) {
    $(document).ready(function () {
        const divOfMain = $("#navBar");
        const uiPathMapping = {
            "home": {
                "default": "/",
                "include": ["/"]
            },
            "mySpace": {
                "default": "/myspace/",
                "include": ["/myspace/", "/myspace/course/"]
            }
        };

        initDivOfNavBar();

        const loginDialogJS = "/content/javascript/logindialog.js"
        loadAsScript(loginDialogJS);

        function initDivOfNavBar() {
            $.get("/nav/nav", function (result) {
                navTemplate = $(result)[0];
                divOfMain.append(navTemplate);

                initUiPath();
                initSelection();
            })

            function initUiPath() {
                for (const key in uiPathMapping) {
                    $("#" + key).attr("href", uiPathMapping[key].default);
                }
            }

            function initSelection() {
                const pathPattern = /((?:\/\w+\/)(?:\w+\/)?)/
                const path = window.location.pathname;

                const prefix = pathPattern.exec(path);
                let isFound = false;
                if (prefix != null) {
                    for (const uiPathKey in uiPathMapping) {
                        if (uiPathMapping[uiPathKey].include.includes(prefix[0])) {
                            $("#" + uiPathKey).parent().addClass("active");
                            isFound = true;
                            return;
                        }
                    }
                }
                if (!isFound) {
                    //set home as default
                    $("#home").parent().addClass("active");
                }
            }
        }
    })
})