const apiKey = "123456";

function getEmail(accessToken, callback) {
    if (accessToken) {
        const endPoint = "/api/email";
        const url = `${endPoint}`;
        const setting = {
            "async": true,
            "url": url,
            "method": "POST",
            "headers": {
                "api_key": apiKey,
                "cache-control": "no-cache"
            },
            "data": {
                "canvasAccessToken": accessToken
            }
        }
        return ajaxCall(setting, callback);
    }
    else {
        return callback();
    }
}

function signUp(email, accessToken, password, callback) {
    if (email && accessToken && password) {
        const endPoint = "/api/signup";
        const url = `${endPoint}`;
        const setting = {
            "async": true,
            "url": url,
            "method": "POST",
            "headers": {
                "api_key": apiKey,
                "cache-control": "no-cache"
            },
            "data": {
                "email": email,
                "canvasAccessToken": accessToken,
                "password": password
            }
        }
        return ajaxCall(setting, callback);
    }
    else {
        return callback();
    }
}

function getUserName(jwt, callback) {
    if (jwt) {
        const endPoint = "/api/user";
        const url = `${endPoint}`;
        const setting = {
            "async": true,
            "url": url,
            "method": "POST",
            "headers": {
                "api_key": apiKey,
                "cache-control": "no-cache"
            },
            "data": {
                "jwt": jwt
            }
        }
        return ajaxCall(setting, callback);
    }
    else {
        return callback();
    }
}

function getCourse(jwt, callback) {
    if (jwt) {
        const endPoint = `/api/course`;
        const url = `${endPoint}?jwt=${jwt}`;
        const setting = {
            "async": true,
            "url": url,
            "method": "GET",
            "headers": {
                "api_key": apiKey,
                "cache-control": "no-cache"
            }
        }
        return ajaxCall(setting, callback);
    }
    else {
        return callback();
    }
}

function getCourseById(jwt, courseId, callback) {
    if (jwt && courseId) {
        const endPoint = `/api/getCourseById`;
        const url = `${endPoint}?jwt=${jwt}&courseId=${courseId}`;
        const setting = {
            "async": true,
            "url": url,
            "method": "GET",
            "headers": {
                "api_key": apiKey,
                "cache-control": "no-cache"
            }
        }
        return ajaxCall(setting, callback);
    }
    else {
        return callback();
    }
}

function getUserOfCourse(jwt, courseId, callback) {
    if (jwt && courseId) {
        const endPoint = `/api/getUserOfCourse`;
        const url = `${endPoint}?jwt=${jwt}&courseId=${courseId}`;
        const setting = {
            "async": true,
            "url": url,
            "method": "GET",
            "headers": {
                "api_key": apiKey,
                "cache-control": "no-cache"
            }
        }
        return ajaxCall(setting, callback);
    }
    else {
        return callback();
    }
}

function getGradeColumn(jwt, courseId, callback) {
    if (jwt && courseId) {
        const endPoint = `/api/gradeColumns`;
        const url = `${endPoint}?jwt=${jwt}&courseId=${courseId}`;
        const setting = {
            "async": true,
            "url": url,
            "method": "GET",
            "headers": {
                "api_key": apiKey,
                "cache-control": "no-cache"
            }
        }
        return ajaxCall(setting, callback);
    }
    else {
        return callback();
    }
}

function getGrade(jwt, courseId, studentIDArrayString, callback) {
    if (jwt && courseId && studentIDArrayString) {
        const endPoint = `/api/grade`;
        const url = `${endPoint}?jwt=${jwt}&courseId=${courseId}&studentId=${studentIDArrayString}`;
        const setting = {
            "async": true,
            "url": url,
            "method": "GET",
            "headers": {
                "api_key": apiKey,
                "cache-control": "no-cache"
            }
        }
        return ajaxCall(setting, callback);
    }
    else {
        return callback();
    }
}

function postConversation(jwt, courseId, studentIDArrayString, subject, body, callback) {
    if (jwt && courseId && studentIDArrayString) {
        const endPoint = `/api/conversation`;
        const url = `${endPoint}?jwt=${jwt}&courseId=${courseId}&studentId=${studentIDArrayString}&subject=${subject}&body=${body}`;
        const setting = {
            "async": true,
            "url": url,
            "method": "POST",
            "headers": {
                "api_key": apiKey,
                "cache-control": "no-cache"
            }
        }
        return ajaxCall(setting, callback);
    }
    else {
        return callback();
    }
}


function ajaxCall(setting, callback) {
    $.ajax(setting).done(function (response) {
        callback(response);
    });
}