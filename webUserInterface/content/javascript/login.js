//this js aims to handle the login logic
bindLoginButton();

function bindLoginButton() {
    const loginButtonElement = $("#dialogLogin");
    loginButtonElement.click(function (event) {
        event.preventDefault();
        loginUser(function (isSuccessful) {
            if (isSuccessful) {
                //refresh the page
                console.log("success");
                toastr["success"]("Welcome back!");
                window.setTimeout(() => {
                    location.reload();
                }, 500);
            } else {
                //TODO: update the UI
                toastr["warning"]("Email or Passwors is not correct!")
                console.log("unable to login");
            }
        });
    });
}

function loginUser(callback) {
    const email = $("#usrname").val();
    const password = $("#psw").val();
    if (email && password) {
        checkKeratinReady(function () {
            const data = {
                username: email,
                password: password
            };
            KeratinAuthN.login(data).then(function (result) {
                callback(true);
            }).catch(function (error) {
                console.log(error);
                callback(false);
            });
        });
    }
}
