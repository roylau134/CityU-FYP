FROM node:8
MAINTAINER Roy Lau

# Copy project
COPY swaggerapi/ /home/Express-Api-SwaggerUi

# Update npm and install knex for DB migration
RUN npm install -g npm@latest && npm install knex -g

# Install npm modules
RUN (cd /home/Express-Api-SwaggerUi && npm install)

# start db migrate and then start the app
CMD (cd /home/Express-Api-SwaggerUi && knex migrate:latest && npm start --prefix /home/Express-Api-SwaggerUi)