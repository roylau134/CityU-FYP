-------------install docker on ubuntu 16.xx--------
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
apt-cache policy docker-ce
sudo apt-get install -y docker-ce
-----------install docker compose---------
sudo -i
curl -L https://github.com/docker/compose/releases/download/1.15.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version

docker-compose run [app name] bash

docker-compose up
docker-compose down
------------------------------------------
docker ps //list [CONTAINER_ID]

docker exec -it [CONTAINER_ID] bash
docker exec -it  bash

docker commit [container id] [new image name]

docker image ls
docker images --filter "dangling=true" -q --no-trunc

docker rmi [image name]
docker rmi [REPOSITORY]:[TAG]


#To show all containers use the given command:
docker ps -a
docker rm [container id]
docker start/stop/restart [container id]

docker pull nginx
docker run --name docker-nginx -p 80:80 nginx

-------------------------push to ECR---------------------------
eval $(aws ecr get-login --no-include-email --region ap-southeast-1 | sed 's|https://||')

docker tag docker_dev_abtestapi:latest 828273753143.dkr.ecr.ap-southeast-1.amazonaws.com/testabtest:latest

docker push 828273753143.dkr.ecr.ap-southeast-1.amazonaws.com/testabtest:latest














