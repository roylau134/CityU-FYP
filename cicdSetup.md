# Cicd setup guide
the follow is required to use a Linux to perform as a runner.

``` sh
#install gitlab-runner https://docs.gitlab.com/runner/install/linux-manually.html#install
sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner

#apt-get update
apt-get update

#install docker
apt-get install -y  \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
	
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"   
apt-get update
apt-get install -y docker-ce
apt-cache madison docker-ce

### install docker compose
curl -L https://github.com/docker/compose/releases/download/1.15.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

#test docker is ready
docker run hello-world

#config runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

#run runner
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

#Registering Runners https://docs.gitlab.com/runner/register/index.html#gnu-linux
#then follow the above URL to register the runner
#please select executor as shell
sudo gitlab-runner register

#update sudo config 
sudo nano /etc/sudoers
#under "# User privilege specification"
#Add this line "gitlab-runner ALL=(ALL) NOPASSWD: ALL" 

#update vm config
sudo usermod -aG docker gitlab-runner
sysctl -w vm.max_map_count=262144
gitlab-runner restart
```